# Spaho

Fork of [Zdog](https://github.com/metafizzy/zdog), [Zfont](https://github.com/jaames/zfont) and [Typr](https://github.com/photopea/Typr.js).


## Codeberg
https://codeberg.org/sallecta/spaho

## Site
https://sallecta.codeberg.page/spaho
