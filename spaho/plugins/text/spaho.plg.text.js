/*!
 * Spaho plg.text
 * Copyright 2023 Alexander Gribkov
 * https://codeberg.org/sallecta/spaho
 * Licensed MIT
 * 
 * Based on
 * Zfont v1.2.8
 * Text plugin for Zdog
 * 2019 James Daniel
 * MIT Licensed 
 * github.com/jaames/zfont
 */
//"use strict";
document.addEventListener(
	spaho.events.included.name,
	function( a_ev )
	{
		const plg={};
		plg.key='text';
		plg.name=spaho.name+'.'+plg.key;
		plg.defs={};
		plg.defs.path = spaho.dirname(this.bind_curr_script);
		plg.defs.regexp = /\r?\n/;
		//
		Object.freeze(plg.defs);
		/**/
		plg.cfg={};
		plg.cfg.factoryfont="SourceSans3_Regular_ttf";
		/**/
		plg.b64fnts={};
		const core = spaho.mdls.core;
		const lfont= spaho.libs.font;
		plg.FntList = [];
		// Helper to wait for all fonts to load
		plg.waitForFonts = function()
		{
			return Promise.all(
				plg.FntList.map(
					function (font)
					{
						return font.waitForLoad();
					}
				)
			);
		};
		/* 
		 * Font 
		*/
		plg.Font = function (a_props)
		{
			var this$1 = this;//?????????!!!!!!!
			// Set missing a_props to default values
			a_props = spaho.mdls.core.extend(
				{
					src: '',
				},
				a_props
			);
			this.src = a_props.src;
			this.font = null;
			this._hasLoaded = false;
			this._loadCallbacks = [];
			// Add this font instance to the internal font list
			plg.FntList.push(this);
			// Begin loading font file
			this._fetchFontResource(this.src)
			.then(function (a_buffer)
				{
					var font = lfont.parse(a_buffer);
					// check font fields to see if the font was parsed correctly
					if ((!font.head) || (!font.hmtx) || (!font.hhea) || (!font.glyf))
					{
						// get a list of missing font fields
						var missingFields = ['head', 'hmtx', 'hhea', 'glyf']
						.filter(function (field) { return !font[field]; });
						throw new Error(("could not parse this font (unable to find " + (missingFields.join(', ')) + ")"));
					}
					return font;
				}
			)
			.then(function (font)
				{
					this$1.font = font;
					this$1._hasLoaded = true;
					this$1._loadCallbacks.forEach(function (callback) { return callback(); });
				}
			)
			.catch(function (err)
				{
					console.warn('Unable to load font',(this$1.src));
					this$1._fetchFactoryFont()
					.then(function (a_buffer)
						{
							var font = lfont.parse(a_buffer);
							// check font fields to see if the font was parsed correctly
							if ((!font.head) || (!font.hmtx) || (!font.hhea) || (!font.glyf))
							{
								// get a list of missing font fields
								var missingFields = ['head', 'hmtx', 'hhea', 'glyf']
								.filter(function (field) { return !font[field]; });
								throw new Error(("could not parse this font (unable to find " + (missingFields.join(', ')) + ")"));
							}
							return font;
						}
					)
					.then(function (font)
						{
							this$1.font = font;
							this$1._hasLoaded = true;
							this$1._loadCallbacks.forEach(function (callback) { return callback(); });
						}
					)
					.catch(function (err)
					{
						throw new Error("Unable to load factory font " + plg.cfg.factoryfont);
					});
				}
			);
		};
		plg.Font.prototype.waitForLoad = function waitForLoad ()
		{
			var this$1 = this;
			return new Promise(function (resolve, reject)
				{
					// If the font is loaded, we can resolve right away
					if (this$1._hasLoaded && this$1._hasLoaded) //??!!
					{
						resolve();
					}
					// Otherwise, wait for it to load
					else
					{
						this$1._loadCallbacks.push(resolve);
					}
				}
			);
		};
		plg.Font.prototype.getFontScale = function getFontScale (a_size)
		{
			if (!this._hasLoaded)
			{
				return null;
			}
			else
			{
				return 1 / this.font.head.unitsPerEm * a_size;
			}
		};
		plg.Font.prototype.measureText = function measureText (text, fontSize)
		{
			var this$1 = this;
			if ( fontSize === void 0 ) fontSize=64;
			if (!this._hasLoaded)
			{
				return null;
			}
			var lines = Array.isArray(text) ? text : text.split(plg.defs.regexp);
			var font = this.font;
			var advanceWidthTable = font.hmtx.aWidth;
			var fontScale = this.getFontScale(fontSize);
			var descender = font.hhea.descender;
			var ascender = font.hhea.ascender;
			var lineGap = font.hhea.lineGap;
			var lineWidths = lines.map(function (line)
				{
					var glyphs = lfont.U.stringToGlyphs(this$1.font, line);
					return glyphs.reduce(function (advanceWidth, glyphId)
						{
							// stringToGlyphs returns an array on glyph IDs that is the same length as the text string
							// an ID can sometimes be -1 in cases where multiple characters are merged into a single ligature
							if (glyphId > -1 && glyphId < advanceWidthTable.length)
							{
								advanceWidth += advanceWidthTable[glyphId];
							}
							return advanceWidth;
						},
						0
					);
				}
			);
			var width = Math.max.apply(Math, lineWidths);
			var lineHeight = (0 - descender) + ascender;
			var height = lineHeight * lines.length;
			// Multiply by fontScale to convert from font units to pixels
			return{
				width: width * fontScale,
				height: height * fontScale,
				lineHeight: lineHeight * fontScale,
				lineWidths: lineWidths.map(function (width) { return width * fontScale; }),
				descender: descender * fontScale,
				ascender: ascender * fontScale,
			};
		}; // plg.Font.prototype.measureText
		plg.Font.prototype.getTextPath = function getTextPath (text, fontSize, x, y, z, alignX, alignY)
		{
			var this$1 = this;
			if ( fontSize === void 0 ) fontSize=64;
			if ( x === void 0 ) x=0;
			if ( y === void 0 ) y=0;
			if ( z === void 0 ) z=0;
			if ( alignX === void 0 )
			{alignX='left';}
			if ( alignY === void 0 )
			{alignY='bottom';}
			if (!this._hasLoaded)
			{
				return [];
			}
			var lines = Array.isArray(text) ? text : text.split(plg.defs.regexp);
			var measurements = this.measureText(text, fontSize);
			var lineWidths = measurements.lineWidths;
			var lineHeight = measurements.lineHeight;
			return lines.map(function (line, lineIndex)
				{
					var ref = this$1.getTextOrigin(
						Object.assign(
								{},
								measurements,
								{width: lineWidths[lineIndex]}
						),
						x, y, z, alignX, alignY
					);
					var _x = ref[0];
					var _y = ref[1];
					var _z = ref[2];
					y += lineHeight;
					var glyphs = lfont.U.stringToGlyphs(this$1.font, line);
					var path = lfont.U.glyphsToPath(this$1.font, glyphs);
					return this$1._convertPathCommands(path, fontSize, _x, _y, z);
				}
			)
			.flat();
		};//plg.Font.prototype.getTextPath
		plg.Font.prototype.getTextGlyphs = function getTextGlyphs (text, fontSize, x, y, z, alignX, alignY)
		{
			var this$1 = this;
			if ( fontSize === void 0 )
			{ fontSize=64; }
			if ( x === void 0 )
			{ x=0; }
			if ( y === void 0 )
			{ y=0; }
			if ( z === void 0 )
			{ z=0; }
			if ( alignX === void 0 )
			{ alignX='left'; }
			if ( alignY === void 0 )
			{ alignY='bottom'; }
			if (!this._hasLoaded)
			{ return []; }
			var measurements = this.measureText(text, fontSize);
			var advanceWidthTable = this.font.hmtx.aWidth;
			var fontScale = this.getFontScale(fontSize);
			var lineWidths = measurements.lineWidths;
			var lineHeight = measurements.lineHeight;
			var lines = Array.isArray(text) ? text : text.split(plg.defs.regexp);
			return lines.map(function (line, lineIndex)
				{
					var glyphs = lfont.U.stringToGlyphs(this$1.font, line);
					var ref = this$1.getTextOrigin(
						Object.assign(
							{},
							measurements,
							{width: lineWidths[lineIndex]}
						),
						x,
						y,
						z,
						alignX,
						alignY
					);
					var _x = ref[0];
					var _y = ref[1];
					var _z = ref[2];
					y += lineHeight;
					return glyphs.filter(
						function (glyph) { return glyph !== -1; }
					)
					.map(function (glyphId)
						{
							var path = lfont.U.glyphToPath(this$1.font, glyphId);
							var shape = {
								translate: {x:_x, y: _y, z:_z},
								path: this$1._convertPathCommands(path, fontSize, 0, 0, 0)
							};
							_x += advanceWidthTable[glyphId] * fontScale;
							return shape;
						}
					);
				}
			).flat(); 
		};//plg.Font.prototype.getTextGlyphs
		plg.Font.prototype.getTextOrigin = function getTextOrigin (measuement, x, y, z, alignX, alignY)
		{
			if ( x === void 0 )
			{ x=0; }
			if ( y === void 0 )
			{ y=0; }
			if ( z === void 0 )
			{ z=0; }
			if ( alignX === void 0 )
			{ alignX='left'; }
			if ( alignY === void 0 )
			{ alignY='bottom'; }
			var width = measuement.width;
			var height = measuement.height;
			var lineHeight = measuement.lineHeight;
			switch (alignX)
			{
				case 'right':
					x -= width;
					break;
				case 'center':
					x -= width / 2;
					break;
			}
			switch (alignY)
			{
				case 'middle':
					y -= (height / 2)- lineHeight;
					break;
				case 'bottom':
				default:
					y -= height - lineHeight;
					break;
			}
			return [x, y, z];
		};//plg.Font.prototype.getTextOrigin
		// Convert lfont.js path commands to core commands
		// Also apply font size scaling and coordinate adjustment
		// https://github.com/photopea/lfont.js
		// https://zzz.dog/shapes#shape-path-commands
		plg.Font.prototype._convertPathCommands = function _convertPathCommands (path, fontSize, x, y, z)
		{
			if ( x === void 0 )
			{ x=0; }
			if ( y === void 0 )
			{ y=0; }
			if ( z === void 0 )
			{ z=0; }
			var yDir = -1;
			var xDir = 1;
			var fontScale = this.getFontScale(fontSize);
			var commands = path.cmds;
			// Apply font scale to all coords
			var coords = path.crds.map(function (coord) { return coord * fontScale; });
			// Convert coords to core commands
			var startCoord = null;
			var coordOffset = 0;
			return commands.map(function (cmd)
				{
					var result = null;
					if (!startCoord)
					{
						startCoord = {x: x + coords[coordOffset] * xDir, y: y + coords[coordOffset + 1] * yDir, z: z};
					}
					switch (cmd)
					{
						case 'M': // moveTo command
							result = {
								move:
									{
										x: x + coords[coordOffset] * xDir,
										y: y + coords[coordOffset + 1] * yDir,
										z: z
									}
							};
							coordOffset += 2;
							return result;
						case 'L': // lineTo command
							result = 
							{
								line:
								{
									x: x + coords[coordOffset] * xDir,
									y: y + coords[coordOffset + 1] * yDir,
									z: z
								}
							};
							coordOffset += 2;
							return result;
						case 'C': // curveTo command
							result = {
								bezier:
								[
									{x: x + coords[coordOffset]   * xDir, y: y + coords[coordOffset + 1] * yDir, z: z},
									{x: x + coords[coordOffset + 2] * xDir, y: y + coords[coordOffset + 3] * yDir, z: z},
									{x: x + coords[coordOffset + 4] * xDir, y: y + coords[coordOffset + 5] * yDir, z: z}
								]
							};
							coordOffset += 6;
							return result;
						case 'Q': // arcTo command
							result = {
								arc:
								[
									{x: x + coords[coordOffset]   * xDir, y: y + coords[coordOffset + 1] * yDir, z: z},
									{x: x + coords[coordOffset + 2] * xDir, y: y + coords[coordOffset + 3] * yDir, z: z}
								]
							};
							coordOffset += 4;
							return result;
						case 'Z': // close path
							if (startCoord)
							{
								result = {
									line: startCoord
								};
								startCoord = null;
							}
							return result;
						// unhandled type
						// currently, #rrggbb and X types (used in multicolor fonts) aren't supported
						default:
							return result;
					}
				}
			).filter(function (cmd) { return cmd !== null; }); // filter out null commands
		};//plg.Font.prototype._convertPathCommands
		plg.Font.prototype._fetchFontResource = function _fetchFontResource (source)
		{
			return new Promise(
				function (resolve, reject)
				{
					//console.log('Loading font via hxr.');
					var request = new XMLHttpRequest();
					// Fetch as an arrayBuffer for lfont.parse
					request.responseType = 'arraybuffer'; 
					request.open('GET', source, true);
					request.onreadystatechange = function (e)
					{
						if (request.readyState === 4)
						{
							if (request.status >= 200 && request.status < 300)
							{
								resolve(request.response);
							}
							else
							{
								reject((request.status)+': '+(request.statusText));
							}
						}
					};
					request.send(null);
				}
			);
		};//plg.Font.prototype._fetchFontResource
		plg.Font.prototype._fetchFactoryFont = function _fetchFactoryFont ()
		{
			function base64ToArrayBuffer(a_b64str)
			{
				var binaryString = atob(a_b64str);
				var bytes = new Uint8Array(binaryString.length);
				for (var i = 0; i < binaryString.length; i++)
				{
					bytes[i] = binaryString.charCodeAt(i);
				}
				return bytes.buffer;
			}
			return new Promise(
				function (resolve, reject)
				{
					const pfnt=plg.defs.path+'/fonts_b64/'+plg.cfg.factoryfont+'.js';
					//console.log('Loading b64 font via createElement.');
					el = document.createElement('script');
					el.src = pfnt; 
					el.type = 'text/javascript';
					function checkresult(a_ev)
					{
						if ( a_ev.type === 'load')
						{
							//console.log('loaded factory font',pfnt);
							console.warn('Using factory b64 font',plg.cfg.factoryfont);
							const binString = atob(plg.b64fnts.SourceSans3_Regular_ttf);
							resolve(base64ToArrayBuffer(plg.b64fnts.SourceSans3_Regular_ttf));
						}
						else if ( a_ev.type === 'error' )
						{
							const msg='error loading factory font';
							console.error(msg,pfnt);
							reject(msg,pfnt);
						}
						else
						{
							console.error('unknown loading result of',pfnt,a_ev.type);
							reject('unknown loading result of',pfnt,a_ev.type);
						}
					}
					el.addEventListener('error',checkresult);
					el.addEventListener('load',checkresult);
					document.head.appendChild(el);
				}
			);
		};//plg.Font.prototype._fetchFactoryFont
		/* 
		 * Str 
		*/
		function objectWithoutProperties (a_obj, a_exclude)
		{
			var target = {}; 
			for (var k in a_obj)
			{
				if (Object.prototype.hasOwnProperty.call(a_obj, k) && a_exclude.indexOf(k) === -1)
				{
					target[k] = a_obj[k]; 
				}
			}
			return target;
		}
		plg.Str= function (a_props) // called by user
		{
			// Set missing a_props to default values
			a_props = core.extend(
				{
					font: null,
					value: '',
					fontSize: 64,
					textAlign: 'left',
					textBaseline: 'bottom',
				},
				a_props
			);
			// Split a_props
			var font = a_props.font;
			var value = a_props.value;
			var fontSize = a_props.fontSize;
			var textAlign = a_props.textAlign;
			var textBaseline = a_props.textBaseline;
			var rest = objectWithoutProperties( a_props, ["font", "value", "fontSize", "textAlign", "textBaseline"] );
			var shapeProps = rest;
			// Create shape object
			core.Shape.call(
				this, 
				Object.assign(
					{},
					shapeProps,
					{
						closed: true,
						visible: false, // hide until font is loaded
						path: [{}]
					}
				)
			);
			this._font = null;
			this._value = value;
			this._fontSize = fontSize;
			this._textAlign = textAlign;
			this._textBaseline = textBaseline;
			this.font = font;
		}
		plg.Str.__proto__ = core.Shape;
		plg.Str.prototype = Object.create( core.Shape && core.Shape.prototype );
		plg.Str.prototype.constructor = plg.Str;
		var str_props = 
		{
			font: { configurable: true },
			value: { configurable: true },
			fontSize: { configurable: true },
			textAlign: { configurable: true },
			textBaseline: { configurable: true } 
		};
		plg.Str.prototype.updateText = function updateText ()
		{
			var path = this.font.getTextPath(this.value, this.fontSize, 0, 0, 0, this.textAlign, this.textBaseline);
			if (path.length == 0)
			{ // zdog doesn't know what to do with empty path arrays
				this.path = [{}];
				this.visible = false;
			}
			else
			{
				this.path = path;
				this.visible = true;
			}
			this.updatePath();
		};
		str_props.font.set = function (newFont)
		{
			var this$1 = this;
			this._font = newFont;
			this.font.waitForLoad().then(function ()
				{
					this$1.updateText();
					this$1.visible = true;
					// Find root core.Illustration instance
					var root = this$1.addTo;
					while (root.addTo !== undefined)
					{
						root = root.addTo;
					}
					// Update render graph
					if (root && typeof root.updateRenderGraph === 'function')
					{
						root.updateRenderGraph();
					}
				}
			);
		};
		str_props.font.get = function ()
		{
			return this._font;
		};
		str_props.value.set = function (newValue)
		{
			this._value = newValue;
			this.updateText();
		};
		str_props.value.get = function ()
		{
			return this._value;
		};
		str_props.fontSize.set = function (newSize)
		{
			this._fontSize = newSize;
			this.updateText();
		};
		str_props.fontSize.get = function ()
		{
			return this._fontSize;
		};
		str_props.textAlign.set = function (newValue)
		{
			this._textAlign = newValue;
			this.updateText();
		};
		str_props.textAlign.get = function ()
		{
			return this._textAlign;
		};
		str_props.textBaseline.set = function (newValue)
		{
			this._textBaseline = newValue;
			this.updateText();
		};
		str_props.textBaseline.get = function ()
		{
			return this._textBaseline;
		};
		Object.defineProperties( plg.Str.prototype, str_props );
		
		plg.Str.optionKeys = plg.Str.optionKeys.concat(['font', 'fontSize', 'value', 'textAlign', 'textBaseline']);
		
		/*
		 * Group
		 * */
		plg.Group=function(a_props)
		{
			// Set missing a_props to default values
			a_props = core.extend(
				{
					font: null,
					value: '',
					fontSize: 64,
					textAlign: 'left',
					textBaseline: 'bottom',
					color: '#333',
					fill: false,
					stroke: 1,
				},
				a_props
			);
			// Split a_props
			var font = a_props.font;
			var value = a_props.value;
			var fontSize = a_props.fontSize;
			var textAlign = a_props.textAlign;
			var textBaseline = a_props.textBaseline;
			var color = a_props.color;
			var fill = a_props.fill;
			var stroke = a_props.stroke;
			var rest = objectWithoutProperties(
				a_props,
				["font", "value", "fontSize", "textAlign", "textBaseline", "color", "fill", "stroke"] 
			);
			var groupProps = rest;
			// Create group object
			core.Group.call(this, Object.assign({}, groupProps,
			{visible: false}));
			this._font = null;
			this._value = value;
			this._fontSize = fontSize;
			this._textAlign = textAlign;
			this._textBaseline = textBaseline;
			this._color = color;
			this._fill = fill;
			this._stroke = stroke;
			this.font = font;
		}
		plg.Group.__proto__ = core.Group;
		plg.Group.prototype = Object.create( core.Group && core.Group.prototype );
		plg.Group.prototype.constructor = plg.Group;
		var group_props =
		{
			font: { configurable: true },
			value: { configurable: true },
			fontSize: { configurable: true },
			textAlign: { configurable: true },
			textBaseline: { configurable: true },
			color: { configurable: true },
			fill: { configurable: true },
			stroke: { configurable: true } 
		};
		plg.Group.prototype.updateText = function updateText ()
		{
			var this$1 = this;
			// Remove old children
			while (this.children.length > 0)
			{
				this.removeChild(this.children[0]);
			}
			// Get text paths for each glyph
			var glyphs = this.font.getTextGlyphs(this.value, this.fontSize, 0, 0, 0, this.textAlign, this.textBaseline);
			// Convert glyphs to new shapes
			glyphs.filter(
				function (shape)
				{
					return shape.path.length > 0; 
				}
			).forEach(function (shape)
				{
					this$1.addChild(
						new core.Shape(
							{
								translate: shape.translate,
								path: shape.path,
								color: this$1.color,
								fill: this$1.fill,
								stroke: this$1.stroke,
								closed: true
							}
						)
					);
				}
			);
			this.updateFlatGraph();
		};
		group_props.font.set = function (a_fnt)
		{
			var this$1 = this;
			this._font = a_fnt;
			this._font.waitForLoad().then(function ()
				{
					this$1.updateText();
					this$1.visible = true;
					// Find root core.Illustration instance
					var root = this$1.addTo;
					while (root.addTo !== undefined)
					{
						root = root.addTo;
					}
					// Update render graph
					if (root && typeof root.updateRenderGraph === 'function')
					{
						root.updateRenderGraph();
					}
				}
			);
		};
		group_props.font.get = function ()
		{
			return this._font;
		};
		group_props.value.set = function (a_val)
		{
			this._value = a_val;
			this.updateText();
		};
		group_props.value.get = function ()
		{
			return this._value;
		};
		group_props.fontSize.set = function (a_sz)
		{
			this._fontSize = a_sz;
			this.updateText();
		};
		group_props.fontSize.get = function ()
		{
			return this._fontSize;
		};
		group_props.textAlign.set = function (a_val)
		{
			this._textAlign = a_val;
			this.updateText();
		};
		group_props.textAlign.get = function ()
		{
			return this._textAlign;
		};
		group_props.textBaseline.set = function (a_val)
		{
			this._textBaseline = a_val;
			this.updateText();
		};
		group_props.textBaseline.get = function ()
		{
			return this._textBaseline;
		};
		group_props.color.set = function (a_color)
		{
			this._color = a_color;
			this.children.forEach(
				function (child)
				{
					return child.color = a_color;
				}
			);
		};
		group_props.color.get = function ()
		{
			return this._color;
		};
		group_props.fill.set = function (a_fill)
		{
			this._fill = a_fill;
			this.children.forEach(
				function (child)
				{
					return child.fill = a_fill;
				}
			);
		};
		group_props.fill.get = function ()
		{
			return this._fill;
		};
		group_props.stroke.set = function (a_stroke)
		{
			this._stroke = a_stroke;
			this.children.forEach(
				function (child)
				{
					return child.stroke = a_stroke;
				}
			);
		};
		group_props.stroke.get = function ()
		{
			return this._stroke;
		};
		Object.defineProperties( plg.Group.prototype, group_props );
		plg.Group.optionKeys = plg.Group.optionKeys.concat(
			['color', 'fill', 'stroke', 'font', 'fontSize', 'value', 'textAlign', 'textBaseline']
		);
		plg.Font.list = [];
		plg.Font.waitForFonts = function() //??
		{
			return Promise.all(
				plg.Font.list.map(
					function (a_font)
					{
						return a_font.waitForLoad();
					}
				)
			);
		};
		/*Zdog compatability*/
		window.Zfont=plg;
		window.Zfont.init=function(){};
		Zdog.Font=plg.Font;
		Zdog.Text=plg.Str;
		/*end Zdog compatability*/
		/**/
		const me = plg.name;
		spaho.plgs[plg.key] = plg;
		//console.log(me,'added self to spaho.plgs.'+plg.key,spaho.plgs[plg.key],);
		document.dispatchEvent(spaho.events.loaded);
		/**/
	}.bind({bind_curr_script:window.document.currentScript.src})
);
