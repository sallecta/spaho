/*!
 * Spaho mdl.core
 * Copyright 2023 Alexander Gribkov
 * https://codeberg.org/sallecta/spaho
 * Licensed MIT
 * Based on
 * 
 * Zdog v1.1.3
 * Round, flat, designer-friendly pseudo-3D engine
 * Licensed MIT
 * https://zzz.dog
 * Copyright 2020 Metafizzy
 * 
 * AnchorJS
 * MIT License
 * Copyright (c) 2021 Bryan Braun
 * https://github.com/bryanbraun/anchorjs
 * 
 * perspective feature
 * based on benthillerkus (https://github.com/benthillerkus) comment https://github.com/metafizzy/zdog/issues/2#issuecomment-513565209
 * and retani (https://github.com/retani) commnit https://github.com/intergestalt/zdog/commit/3c325059882c7f7a63b09a3a855138ed8443dd26 
**/
"use strict";
document.addEventListener(
	spaho.events.included.name,
	function( a_ev )
	{
		const mdl={};
		mdl.key='core';
		mdl.name=spaho.name+'.'+mdl.key;
		mdl.defs={};
		mdl.defs.path = spaho.dirname(this.bind_curr_script);
		mdl.defs.tau=Math.PI * 2;
		mdl.defs.point1={ x: 1, y: 1, z: 1 };
		mdl.defs.svgURI = 'http://www.w3.org/2000/svg';
		//
		Object.freeze(mdl.defs);
		/**/
		
		mdl.noop=function()
		{
		};
		
		
		mdl.extend = function( a_a, a_b )
		{
			for ( var prop in a_b )
			{
				a_a[ prop ] = a_b[ prop ];
			}
			return a_a;
		};
		mdl.lerp = function( a_a, a_b, a_alpha )
		{
			return ( a_b - a_a ) * a_alpha + a_a;
		};
		mdl.modulo = function( a_num, a_div )
		{
			return ( ( a_num % a_div ) + a_div ) % a_div;
		};
		var powerMultipliers =
		{
			2: function( a ){return a * a;},
			3: function( a ){return a * a * a;},
			4: function( a ){return a * a * a * a;},
			5: function( a ){return a * a * a * a * a;},
		};
		mdl.easeInOut = function( a_alpha, a_power )
		{
			if ( a_power == 1 )
			{ return a_alpha;}
			a_alpha = Math.max( 0, Math.min( 1, a_alpha ) );
			var isFirstHalf = a_alpha < 0.5;
			var slope = isFirstHalf ? a_alpha : 1 - a_alpha;
			slope /= 0.5;
			// make easing steeper with more multiples
			var powerMultiplier = powerMultipliers[ a_power ] || powerMultipliers[2];
			var curve = powerMultiplier( slope );
			curve /= 2;
			return isFirstHalf ? curve : 1 - curve;
		};
		
		mdl.render={};
		/**
		 * CanvasRenderer
		 */
		/** perspective **/
		mdl.render.pscale=function scale(a_z)
		{
			const fov = -150;
			return fov/(fov+a_z);
		};
		/** end persprctive */
		mdl.render.canvas = {};
		mdl.render.canvas.isCanvas= true;
		mdl.render.canvas.begin = function( a_ctx )
		{
			a_ctx.beginPath();
		};
		mdl.render.canvas.move = function( a_ctx, elem, point )
		{
			if ( spaho.cfg.perspective )
			{
				const s = mdl.render.pscale(point.z);
				a_ctx.moveTo( s*point.x, s*point.y );
				return;
			}
			a_ctx.moveTo( point.x, point.y );
		};
		mdl.render.canvas.line = function( a_ctx, a_el, a_point )
		{
			if ( spaho.cfg.perspective )
			{
				const s = mdl.render.pscale(a_point.z);
				a_ctx.lineTo( a_point.x * s, a_point.y * s);
				return;
			}
			a_ctx.lineTo( a_point.x, a_point.y );
		};
		mdl.render.canvas.bezier = function( a_ctx, a_el, a_cp0, a_cp1, a_end )
		{
			if ( spaho.cfg.perspective )
			{
				const s0 = mdl.render.pscale(a_cp0.z);
				const s1 = mdl.render.pscale(a_cp1.z);
				const se = mdl.render.pscale(a_end.z);
				a_ctx.bezierCurveTo( s0 * a_cp0.x, s0 * a_cp0.y, s1*a_cp1.x, s1*a_cp1.y, se*a_end.x, se*a_end.y );
				return;
			}
			a_ctx.bezierCurveTo( a_cp0.x, a_cp0.y, a_cp1.x, a_cp1.y, a_end.x, a_end.y );
		};
		mdl.render.canvas.closePath = function( a_ctx )
		{
			a_ctx.closePath();
		};
		mdl.render.canvas.setPath = function()
		{
			// ntd
		};
		mdl.render.canvas.renderPath = function( a_ctx, a_el, a_path_cmds, a_closed )
		{
			this.begin( a_ctx, a_el );
			a_path_cmds.forEach( function( command )
				{
					command.render( a_ctx, a_el, mdl.render.canvas );
				}
			);
			if ( a_closed )
			{
				this.closePath( a_ctx, a_el );
			}
		};
		mdl.render.canvas.stroke = function( a_ctx, a_el, a_stroke, a_color, a_linew )
		{
			if ( !a_stroke )
			{
				return;
			}
			a_ctx.strokeStyle = a_color;
			a_ctx.lineWidth = a_linew;
			a_ctx.stroke();
		};
		mdl.render.canvas.fill = function( ctx, elem, a_fill, a_color )
		{
			if ( !a_fill )
			{
				return;
			}
			ctx.fillStyle = a_color;
			ctx.fill();
		};
		mdl.render.canvas.end = function() {};
		
		/**
		 * SvgRenderer
		 */
		mdl.render.svg={};
		mdl.render.svg.isSvg= true;
		// round path coordinates to 3 decimals
		mdl.render.svg.round = function( num )
		{
			return Math.round( num * 1000 ) / 1000;
		};
		mdl.render.svg.getPointString = function( a_point )
		{
			const round=mdl.render.svg.round;
			return round( a_point.x ) + ',' + round( a_point.y ) + ' ';
		}
		mdl.render.svg.begin = function()
		{
			//ntd
		};
		mdl.render.svg.move = function( a_svg, a_el, a_point )
		{
			const getPointString=mdl.render.svg.getPointString;
			return 'M' + getPointString( a_point );
		};
		mdl.render.svg.line = function( a_svg, a_el, a_point )
		{
			const getPointString=mdl.render.svg.getPointString;
			return 'L' + getPointString( a_point );
		};
		mdl.render.svg.bezier = function( a_svg, a_el, a_cp0, a_cp1, a_end )
		{
			const getPointString=mdl.render.svg.getPointString;
			return 'C' + getPointString( a_cp0 ) + 
			getPointString( a_cp1 ) + getPointString( a_end );
		};
		mdl.render.svg.closePath = function()
		{
		  return 'Z';
		};
		mdl.render.svg.setPath = function( a_svg, a_el, a_pathValue )
		{
		  a_el.setAttribute( 'd', a_pathValue );
		};
		mdl.render.svg.renderPath = function( a_svg, a_el, a_pathCommands, a_isClosed )
		{
			var pathValue = '';
			a_pathCommands.forEach( function( a_command )
				{
					pathValue += a_command.render( a_svg, a_el, mdl.render.svg );
				}
			);
			if ( a_isClosed )
			{
				pathValue += this.closePath( a_svg, a_el );
			}
			this.setPath( a_svg, a_el, pathValue );
		};
		mdl.render.svg.stroke = function( a_svg, a_el, a_isStroke, a_color, a_linew )
		{
			if ( !a_isStroke )
			{
				return;
			}
			a_el.setAttribute( 'stroke', a_color );
			a_el.setAttribute( 'stroke-width', a_linew );
		};
		mdl.render.svg.fill = function( a_svg, a_el, a_isFill, a_color )
		{
			const fillColor = a_isFill ? a_color : 'none';
			a_el.setAttribute( 'fill', fillColor );
		};
		mdl.render.svg.end = function( a_svg, a_el )
		{
			a_svg.appendChild( a_el );
		};
		/**
		 * Vector
		 */
		mdl.Vector = function( a_pos )
		{
			this.set( a_pos );
		}
		mdl.Vector.prototype.set = function( a_pos )
		{
			this.x = a_pos && a_pos.x || 0;
			this.y = a_pos && a_pos.y || 0;
			this.z = a_pos && a_pos.z || 0;
			return this;
		};
		// set coordinates without sanitizing
		// vec.write({ y: 2 }) only sets y coord
		mdl.Vector.prototype.write = function( a_pos )
		{
			if ( !a_pos )
			{
				return this;
			}
			this.x = a_pos.x != undefined ? a_pos.x : this.x;
			this.y = a_pos.y != undefined ? a_pos.y : this.y;
			this.z = a_pos.z != undefined ? a_pos.z : this.z;
			return this;
		};
		mdl.Vector.prototype.rotate = function( a_rot )
		{
			if ( !a_rot )
			{
				return;
			}
			this.rotateZ( a_rot.z );
			this.rotateY( a_rot.y );
			this.rotateX( a_rot.x );
			return this;
		};
		mdl.Vector.rotateProperty=function( a_vec, a_ang, a_propA, a_propB )
		{
			if ( !a_ang || a_ang % mdl.defs.tau === 0 )
			{
				return;
			}
			var cos = Math.cos( a_ang );
			var sin = Math.sin( a_ang );
			var a = a_vec[ a_propA ];
			var b = a_vec[ a_propB ];
			a_vec[ a_propA ] = a * cos - b * sin;
			a_vec[ a_propB ] = b * cos + a * sin;
		}
		mdl.Vector.prototype.rotateZ = function( a_ang )
		{
			mdl.Vector.rotateProperty( this, a_ang, 'x', 'y' );
		};
		mdl.Vector.prototype.rotateX = function( a_ang )
		{
			mdl.Vector.rotateProperty( this, a_ang, 'y', 'z' );
		};
		mdl.Vector.prototype.rotateY = function( a_ang )
		{
			mdl.Vector.rotateProperty( this, a_ang, 'x', 'z' );
		};
		mdl.Vector.prototype.isSame = function( a_pos )
		{
			if ( !a_pos )
			{
				return false;
			}
			return this.x === a_pos.x && this.y === a_pos.y && this.z === a_pos.z;
		};
		mdl.Vector.prototype.add = function( a_pos )
		{
			if ( !a_pos )
			{
				return this;
			}
			this.x += a_pos.x || 0;
			this.y += a_pos.y || 0;
			this.z += a_pos.z || 0;
			return this;
		};
		mdl.Vector.prototype.subtract = function( a_pos )
		{
			if ( !a_pos )
			{
				return this;
			}
			this.x -= a_pos.x || 0;
			this.y -= a_pos.y || 0;
			this.z -= a_pos.z || 0;
			return this;
		};
		mdl.Vector.prototype.multiply = function( a_pos )
		{
			if ( a_pos == undefined )
			{
				return this;
			}
			if ( typeof a_pos == 'number' )
			{// multiple all values by same number
				this.x *= a_pos;
				this.y *= a_pos;
				this.z *= a_pos;
			}
			else
			{// multiply object
				this.x *= a_pos.x != undefined ? a_pos.x : 1;
				this.y *= a_pos.y != undefined ? a_pos.y : 1;
				this.z *= a_pos.z != undefined ? a_pos.z : 1;
			}
			return this;
		};
		mdl.Vector.prototype.transform = function( a_transl, a_rot, a_scale )
		{
			this.multiply( a_scale );
			this.rotate( a_rot );
			this.add( a_transl );
			return this;
		};
		mdl.Vector.prototype.lerp = function( a_pos, a_alpha )
		{
			this.x = mdl.lerp( this.x, a_pos.x || 0, a_alpha );
			this.y = mdl.lerp( this.y, a_pos.y || 0, a_alpha );
			this.z = mdl.lerp( this.z, a_pos.z || 0, a_alpha );
			return this;
		};
		mdl.Vector.getMagnitudeSqrt=function( a_sum )
		{
			if ( Math.abs( a_sum - 1 ) < 0.00000001 )
			{ // check if a_sum ~= 1 and skip sqrt
				return 1;
			}
			return Math.sqrt( a_sum );
		}
		mdl.Vector.prototype.magnitude = function()
		{
			var sum = this.x * this.x + this.y * this.y + this.z * this.z;
			return mdl.Vector.getMagnitudeSqrt( sum );
		};
		mdl.Vector.prototype.magnitude2d = function()
		{
			var sum = this.x * this.x + this.y * this.y;
			return mdl.Vector.getMagnitudeSqrt( sum );
		};
		mdl.Vector.prototype.copy = function()
		{
			return new mdl.Vector( this );
		};
		/**
		 * Anchor
		 */
		mdl.Anchor=function( a_opts )
		{
			this.create( a_opts || {} );
		}
		mdl.Anchor.defaults = {};
		mdl.Anchor.prototype.create = function( a_opts )
		{
			this.children = [];
			// set defaults & a_opts
			mdl.extend( this, this.constructor.defaults );
			this.setOptions( a_opts );
			// transform
			this.translate = new mdl.Vector( a_opts.translate );
			this.rotate = new mdl.Vector( a_opts.rotate );
			this.scale = new mdl.Vector( mdl.defs.point1 ).multiply( this.scale );
			// origin
			this.origin = new mdl.Vector();
			this.renderOrigin = new mdl.Vector();
			if ( this.addTo )
			{
				this.addTo.addChild( this );
			}
		};
		mdl.Anchor.optionKeys = Object.keys( mdl.Anchor.defaults ).concat(
			[
				'rotate',
				'translate',
				'scale',
				'addTo',
			]
		);
		mdl.Anchor.prototype.setOptions = function( a_opts )
		{
			var optionKeys = this.constructor.optionKeys;
			
			for ( var key in a_opts )
			{
				if ( optionKeys.indexOf( key ) != -1 )
				{
					this[ key ] = a_opts[ key ];
				}
			}
		};
		mdl.Anchor.prototype.addChild = function( a_shape )
		{
			if ( this.children.indexOf( a_shape ) != -1 )
			{
				return;
			}
			a_shape.remove(); // remove previous parent
			a_shape.addTo = this; // keep parent reference
			this.children.push( a_shape );
		};
		mdl.Anchor.prototype.removeChild = function( a_shape )
		{
			var index = this.children.indexOf( a_shape );
			if ( index != -1 )
			{
				this.children.splice( index, 1 );
			}
		};
		mdl.Anchor.prototype.remove = function()
		{
		  if ( this.addTo )
		  {
			this.addTo.removeChild( this );
		  }
		};
		mdl.Anchor.prototype.update = function()
		{
			// update self
			this.reset();
			// update children
			this.children.forEach( function( child )
				{
					child.update();
				}
			);
			this.transform( this.translate, this.rotate, this.scale );
		};
		mdl.Anchor.prototype.reset = function()
		{
			this.renderOrigin.set( this.origin );
		};
		mdl.Anchor.prototype.transform = function( a_tr, a_rot, a_scale )
		{
			this.renderOrigin.transform( a_tr, a_rot, a_scale );
			// transform children
			this.children.forEach( function( a_child )
				{
					a_child.transform( a_tr, a_rot, a_scale );
				}
			);
		};
		mdl.Anchor.prototype.updateGraph = function()
		{
			this.update();
			this.updateFlatGraph();
			this.flatGraph.forEach( function( a_item )
				{
					a_item.updateSortValue();
				}
			);
			// z-sort
			this.flatGraph.sort( mdl.Anchor.shapeSorter );
		};
		mdl.Anchor.shapeSorter = function( a_a, a_b )
		{
			return a_a.sortValue - a_b.sortValue;
		};
		// custom getter to check for flatGraph before using it
		Object.defineProperty( mdl.Anchor.prototype, 'flatGraph',
			{
				get: function()
				{
					if ( !this._flatGraph )
					{
						this.updateFlatGraph();
					}
					return this._flatGraph;
				},
				set: function( a_graph )
				{
					this._flatGraph = a_graph;
				},
			}
		);
		mdl.Anchor.prototype.updateFlatGraph = function()
		{
			this.flatGraph = this.getFlatGraph();
		};
		// return Array of self & all child graph items
		mdl.Anchor.prototype.getFlatGraph = function()
		{
			var flatGraph = [ this ];
			return this.addChildFlatGraph( flatGraph );
		};
		mdl.Anchor.prototype.addChildFlatGraph = function( a_flatGraph )
		{
			this.children.forEach( function( a_child )
				{
					var childFlatGraph = a_child.getFlatGraph();
					Array.prototype.push.apply( a_flatGraph, childFlatGraph );
				}
			);
			return a_flatGraph;
		};
		mdl.Anchor.prototype.updateSortValue = function()
		{
			this.sortValue = this.renderOrigin.z;
		};
		// ----- render ----- //
		mdl.Anchor.prototype.render = function()
		{
			//ntd
		};
		// TODO refactor out mdl.render.canvas so its not a dependency within anchor.js
		mdl.Anchor.prototype.renderGraphCanvas = function( a_ctx )
		{
			if ( !a_ctx )
			{
				throw new Error
				(
					'a_ctx is ' + a_ctx + '. ' +
					'Canvas context required for render. Check .renderGraphCanvas( a_ctx ).'
				);
			}
			this.flatGraph.forEach( function( a_item )
				{
					a_item.render( a_ctx, mdl.render.canvas );
				}
			);
		};
		mdl.Anchor.prototype.renderGraphSvg = function( a_svg )
		{
			if ( !a_svg )
			{
				throw new Error(
					 'a_svg is ' + a_svg + '. ' +
					'SVG required for render. Check .renderGraphSvg( a_svg ).' 
				);
			}
			this.flatGraph.forEach( function( item )
				{
					item.render( a_svg, a_rend_svg );
				}
			);
		};
		mdl.Anchor.prototype.copy = function( a_opts )
		{
			// copy a_opts
			var itemOptions = {};
			var optionKeys = this.constructor.optionKeys;
			optionKeys.forEach(
				function( a_key )
				{
					itemOptions[ a_key ] = this[ a_key ];
				},
				this
			);
			// add set a_opts
			mdl.extend( itemOptions, a_opts );
			var ItemClass = this.constructor;
			return new ItemClass( itemOptions );
		};
		mdl.Anchor.prototype.copyGraph = function( a_opts )
		{
			var clone = this.copy( a_opts );
			this.children.forEach( function( child )
				{
					child.copyGraph({addTo: clone});
				}
			);
			return clone;
		};
		mdl.Anchor.prototype.normalizeRotate = function()
		{
			this.rotate.x = mdl.modulo( this.rotate.x, mdl.defs.tau );
			this.rotate.y = mdl.modulo( this.rotate.y, mdl.defs.tau );
			this.rotate.z = mdl.modulo( this.rotate.z, mdl.defs.tau );
		};
		mdl.Anchor.getSubclass=function( a_class )
		{
			return function( a_defaults )
			{ // ??
				//console.log('a_defaults',a_defaults);
				// create constructor
				const Item=function( a_opts )
				{
					this.create( a_opts || {} );
				}
				Item.prototype = Object.create( a_class.prototype );
				Item.prototype.constructor = Item;
				Item.defaults = mdl.extend( {}, a_class.defaults );
				mdl.extend( Item.defaults, a_defaults );
				// create optionKeys
				Item.optionKeys = a_class.optionKeys.slice( 0 );
				// add defaults keys to optionKeys, dedupe
				Object.keys( Item.defaults ).forEach( function( key )
					{
						if ( !Item.optionKeys.indexOf( key ) != 1 )
						{
							Item.optionKeys.push( key );
						}
					}
				);
				Item.subclass = mdl.Anchor.getSubclass( Item );
				return Item;
			};
		}
		mdl.Anchor.subclass = mdl.Anchor.getSubclass( mdl.Anchor );
		
		/**
		 * Dragger
		 */
		mdl.Dragger = function( a_opts )
		{
			console.log('Dragger');
			this.create( a_opts || {} );
		}
		mdl.Dragger.prototype.create = function( a_opts )
		{
			mdl.Dragger.evname={};
			// event support, default to mouse events
			if ( window.PointerEvent )
			{
				// PointerEvent, Chrome
				mdl.Dragger.evname.down = 'pointerdown';
				mdl.Dragger.evname.move = 'pointermove';
				mdl.Dragger.evname.up = 'pointerup';
			}
			else if ( 'ontouchstart' in window )
			{
				// Touch Events, iOS Safari
				mdl.Dragger.evname.down = 'touchstart';
				mdl.Dragger.evname.move = 'touchmove';
				mdl.Dragger.evname.up = 'touchend';
			}
			else
			{
				mdl.Dragger.evname.down = 'mousedown';
				mdl.Dragger.evname.move = 'mousemove';
				mdl.Dragger.evname.up = 'mouseup';
			}
			this.onDragStart = a_opts.onDragStart || mdl.noop;
			this.onDragMove = a_opts.onDragMove || mdl.noop;
			this.onDragEnd = a_opts.onDragEnd || mdl.noop;
			this.bindDrag( a_opts.startElement );
		};
		mdl.Dragger.prototype.bindDrag = function( a_el )
		{
			a_el = this.getQueryElement( a_el );
			if ( !a_el )
			{
				return;
			}
			// disable browser gestures #53
			a_el.style.touchAction = 'none';
			a_el.addEventListener( mdl.Dragger.evname.down, this );
		};
		mdl.Dragger.prototype.getQueryElement = function( a_el )
		{
			if ( typeof a_el == 'string' )
			{
				a_el = document.querySelector( a_el );
			}
			return a_el;
		};
		mdl.Dragger.prototype.handleEvent = function( event )
		{
			const method = this[ 'on' + event.type ];
			if ( method )
			{
				method.call( this, event );
			}
		};
		mdl.Dragger.prototype.onpointerdown = function( a_ev )
		{
			this.dragStart( a_ev, a_ev );
		};
		mdl.Dragger.prototype.onpointerdown = mdl.Dragger.prototype.onpointerdown;
		mdl.Dragger.prototype.ontouchstart = function( a_ev )
		{
			this.dragStart( a_ev, a_ev.changedTouches[0] );
		};
		mdl.Dragger.prototype.dragStart = function( a_ev, a_pointer )
		{
			a_ev.preventDefault();
			this.dragStartX = a_pointer.pageX;
			this.dragStartY = a_pointer.pageY;
			window.addEventListener( mdl.Dragger.evname.move, this );
			window.addEventListener( mdl.Dragger.evname.up, this );
			this.onDragStart( a_pointer );
		};
		mdl.Dragger.prototype.ontouchmove = function( a_ev )
		{
			// Moved touch may not be first
			this.dragMove( a_ev, a_ev.changedTouches[0] );
		};
		mdl.Dragger.prototype.onpointermove = function( a_ev )
		{
			this.dragMove( a_ev, a_ev );
		};
		mdl.Dragger.prototype.onmousemove = mdl.Dragger.prototype.onpointermove;
		mdl.Dragger.prototype.dragMove = function( a_ev, a_pointer )
		{
			a_ev.preventDefault();
			var moveX = a_pointer.pageX - this.dragStartX;
			var moveY = a_pointer.pageY - this.dragStartY;
			this.onDragMove( a_pointer, moveX, moveY );
		};
		mdl.Dragger.prototype.dragEnd = function( /* event */)
		{
			window.removeEventListener( mdl.Dragger.evname.move, this );
			window.removeEventListener( mdl.Dragger.evname.up, this );
			this.onDragEnd();
		};
		mdl.Dragger.prototype.onmouseup =
		mdl.Dragger.prototype.onpointerup =
		mdl.Dragger.prototype.ontouchend =
		mdl.Dragger.prototype.dragEnd;
		
		/**
		 * Illustration
		 */
		
		mdl.Illustration = mdl.Anchor.subclass(
			{
				element: undefined,
				centered: true,
				zoom: 1,
				dragRotate: false,
				resize: false,
				onPrerender: mdl.noop,
				onDragStart: mdl.noop,
				onDragMove: mdl.noop,
				onDragEnd: mdl.noop,
				onResize: mdl.noop,
			}
		);
		mdl.extend( mdl.Illustration.prototype, mdl.Dragger.prototype );
		mdl.Illustration.prototype.create = function( a_options )
		{
			mdl.Anchor.prototype.create.call( this, a_options );
			mdl.Dragger.prototype.create.call( this, a_options );
			this.setElement( this.element );
			this.setDragRotate( this.dragRotate );
			this.setResize( this.resize );
		};
		mdl.Illustration.prototype.setElement = function( a_el )
		{
			a_el = this.getQueryElement( a_el );
			if ( !a_el )
			{
				throw new Error( 'mdl.Illustration a_el required. Set to ' + a_el );
			}
			var nodeName = a_el.nodeName.toLowerCase();
			if ( nodeName == 'canvas' )
			{
				this.setCanvas( a_el );
			}
			else if ( nodeName == 'svg' )
			{
				this.setSvg( a_el );
			}
		};
		mdl.Illustration.prototype.setSize = function( a_w, a_h )
		{
			a_w = Math.round( a_w );
			a_h = Math.round( a_h );
			if ( this.isCanvas )
			{
				this.setSizeCanvas( a_w, a_h );
			}
			else if ( this.isSvg )
			{
				this.setSizeSvg( a_w, a_h );
			}
		};
		mdl.Illustration.prototype.setResize = function( a_resize )
		{
			this.resize = a_resize;
			// create resize event listener
			if ( !this.resizeListener )
			{
			this.resizeListener = this.onWindowResize.bind( this );
			}
			// add/remove event listener
			if ( a_resize )
			{
				window.addEventListener( 'resize', this.resizeListener );
				this.onWindowResize();
			}
			else
			{
				window.removeEventListener( 'resize', this.resizeListener );
			}
		};
		// TODO debounce this?
		mdl.Illustration.prototype.onWindowResize = function()
		{
			this.setMeasuredSize();
			this.onResize( this.width, this.height );
		};
		mdl.Illustration.prototype.setMeasuredSize = function()
		{
			var width, height;
			var isFullscreen = this.resize == 'fullscreen';
			if ( isFullscreen )
			{
				width = window.innerWidth;
				height = window.innerHeight;
			}
			else
			{
				var rect = this.element.getBoundingClientRect();
				width = rect.width;
				height = rect.height;
			}
			this.setSize( width, height );
		};
		// ----- render ----- //
		mdl.Illustration.prototype.renderGraph = function( a_item )
		{
			if ( this.isCanvas )
			{
				this.renderGraphCanvas( a_item );
			}
			else if ( this.isSvg )
			{
				this.renderGraphSvg( a_item );
			}
		};
		// combo method
		mdl.Illustration.prototype.updateRenderGraph = function( a_item )
		{
			this.updateGraph();
			this.renderGraph( a_item );
		};
		// ----- canvas ----- //
		mdl.Illustration.prototype.setCanvas = function( a_el )
		{
			this.element = a_el;
			this.isCanvas = true;
			// update related properties
			this.ctx = this.element.getContext('2d');
			// set initial size
			this.setSizeCanvas( a_el.width, a_el.height );
		};
		mdl.Illustration.prototype.setSizeCanvas = function( a_w, a_h )
		{
			this.width = a_w;
			this.height = a_h;
			// up-rez for hi-DPI devices
			var pixelRatio = this.pixelRatio = window.devicePixelRatio || 1;
			this.element.width = this.canvasWidth = a_w * pixelRatio;
			this.element.height = this.canvasHeight = a_h * pixelRatio;
			var needsHighPixelRatioSizing = pixelRatio > 1 && !this.resize;
			if ( needsHighPixelRatioSizing )
			{
			this.element.style.width = a_w + 'px';
			this.element.style.height = a_h + 'px';
			}
		};
		mdl.Illustration.prototype.renderGraphCanvas = function( a_item )
		{
			a_item = a_item || this;
			this.prerenderCanvas();
			mdl.Anchor.prototype.renderGraphCanvas.call( a_item, this.ctx );
			this.postrenderCanvas();
		};
		mdl.Illustration.prototype.prerenderCanvas = function()
		{
			const ctx = this.ctx;
			ctx.lineCap = 'round';
			ctx.lineJoin = 'round';
			ctx.clearRect( 0, 0, this.canvasWidth, this.canvasHeight );
			ctx.save();
			if ( this.centered )
			{
				var centerX = this.width / 2 * this.pixelRatio;
				var centerY = this.height / 2 * this.pixelRatio;
				ctx.translate( centerX, centerY );
			}
			var scale = this.pixelRatio * this.zoom;
			ctx.scale( scale, scale );
			this.onPrerender( ctx );
		};
		mdl.Illustration.prototype.postrenderCanvas = function()
		{
			this.ctx.restore();
		};
		// ----- svg ----- //
		mdl.Illustration.prototype.setSvg = function( a_el )
		{
			this.element = a_el;
			this.isSvg = true;
			this.pixelRatio = 1;
			// set initial size from width & height attributes
			var width = a_el.getAttribute('width');
			var height = a_el.getAttribute('height');
			this.setSizeSvg( width, height );
		};
		mdl.Illustration.prototype.setSizeSvg = function( a_w, a_h )
		{
			this.width = a_w;
			this.height = a_h;
			var viewWidth = a_w / this.zoom;
			var viewHeight = a_h / this.zoom;
			var viewX = this.centered ? -viewWidth/2 : 0;
			var viewY = this.centered ? -viewHeight/2 : 0;
			this.element.setAttribute( 'viewBox', viewX + ' ' + viewY + ' ' +
			viewWidth + ' ' + viewHeight );
			if ( this.resize )
			{
				// remove size attributes, let size be determined by viewbox
				this.element.removeAttribute('width');
				this.element.removeAttribute('height');
			}
			else
			{
				this.element.setAttribute( 'width', a_w );
				this.element.setAttribute( 'height', a_h );
			}
		};
		mdl.Illustration.empty = function ( a_el )
		{
			while ( a_el.firstChild )
			{
				a_el.removeChild( a_el.firstChild );
			}
		}
		mdl.Illustration.prototype.renderGraphSvg = function( a_item )
		{
			const item = a_item || this;
			mdl.Illustration.empty( this.element );
			this.onPrerender( this.element );
			mdl.Anchor.prototype.renderGraphSvg.call( item, this.element );
		};
		// ----- drag ----- //
		mdl.Illustration.prototype.setDragRotate = function( a_item )
		{
			if ( !a_item )
			{
				return;
			}
			else if ( a_item === true )
			{
				a_item = this;
			}
			this.dragRotate = a_item;
			
			this.bindDrag( this.element );
		};
		mdl.Illustration.prototype.dragStart = function( /* event, pointer */)
		{
			this.dragStartRX = this.dragRotate.rotate.x;
			this.dragStartRY = this.dragRotate.rotate.y;
			mdl.Dragger.prototype.dragStart.apply( this, arguments );
		};
		mdl.Illustration.prototype.dragMove = function( a_ev, a_pointer )
		{
			var moveX = a_pointer.pageX - this.dragStartX;
			var moveY = a_pointer.pageY - this.dragStartY;
			var displaySize = Math.min( this.width, this.height );
			var moveRY = moveX/displaySize * mdl.defs.tau;
			var moveRX = moveY/displaySize * mdl.defs.tau;
			this.dragRotate.rotate.x = this.dragStartRX - moveRX;
			this.dragRotate.rotate.y = this.dragStartRY - moveRY;
			mdl.Dragger.prototype.dragMove.apply( this, arguments );
		};
		/**
		 * PathCommand
		 */
		mdl.PathCommand = function( a_method, a_points, a_prev_point )
		{
			function mapVectorPoint( a_point )
			{
				if ( a_point instanceof mdl.Vector )
				{
					return a_point;
				}
				else
				{
					return new mdl.Vector( a_point );
				}
			}
			function mapNewVector( a_point )
			{
			  return new mdl.Vector( a_point );
			}
			this.method = a_method;
			this.points = a_points.map( mapVectorPoint );
			this.renderPoints = a_points.map( mapNewVector );
			this.previousPoint = a_prev_point;
			this.endRenderPoint = this.renderPoints[ this.renderPoints.length - 1 ];
			// arc actions come with previous point & corner point
			// but require bezier control a_points
			if ( a_method == 'arc' )
			{
				this.controlPoints = [ new mdl.Vector(), new mdl.Vector() ];
			}
		}
		mdl.PathCommand.prototype.reset = function() {
		  // reset renderPoints back to orignal points position
		  var points = this.points;
		  this.renderPoints.forEach( function( renderPoint, i ) {
		    var point = points[i];
		    renderPoint.set( point );
		  } );
		};
		mdl.PathCommand.prototype.transform = function( translation, rotation, scale ) {
		  this.renderPoints.forEach( function( renderPoint ) {
		    renderPoint.transform( translation, rotation, scale );
		  } );
		};
		mdl.PathCommand.prototype.render = function( a_ctx, a_el, a_rend ) {
		  return this[ this.method ]( a_ctx, a_el, a_rend );
		};
		
		mdl.PathCommand.prototype.move = function( a_ctx, a_el, a_rend ) {
		  return a_rend.move( a_ctx, a_el, this.renderPoints[0] );
		};
		
		mdl.PathCommand.prototype.line = function( a_ctx, a_el, a_rend ) {
		  return a_rend.line( a_ctx, a_el, this.renderPoints[0] );
		};
		mdl.PathCommand.prototype.bezier = function( a_ctx, a_el, a_rend ) {
		  var cp0 = this.renderPoints[0];
		  var cp1 = this.renderPoints[1];
		  var end = this.renderPoints[2];
		  return a_rend.bezier( a_ctx, a_el, cp0, cp1, end );
		};
		mdl.PathCommand.prototype.arc = function( a_ctx, a_el, a_rend )
		{
			const arcHandleLength = 9/16;
			var prev = this.previousPoint;
			var corner = this.renderPoints[0];
			var end = this.renderPoints[1];
			var cp0 = this.controlPoints[0];
			var cp1 = this.controlPoints[1];
			cp0.set( prev ).lerp( corner, arcHandleLength );
			cp1.set( end ).lerp( corner, arcHandleLength );
			return a_rend.bezier( a_ctx, a_el, cp0, cp1, end );
		};
		/**
		 * Shape
		 */
		
		mdl.Shape = mdl.Anchor.subclass(
			{
				stroke: 1,
				fill: false,
				color: '#333',
				closed: true,
				visible: true,
				path: [ {} ],
				front: { z: 1 },
				backface: true,
			}
		);
		mdl.Shape.prototype.create = function( a_opts )
		{
			mdl.Anchor.prototype.create.call( this, a_opts );
			this.updatePath();
			// front
			this.front = new mdl.Vector( a_opts.front || this.front );
			this.renderFront = new mdl.Vector( this.front );
			this.renderNormal = new mdl.Vector();
		};
		mdl.Shape.prototype.updatePath = function()
		{
			this.setPath();
			this.updatePathCommands();
		};
		// place holder for Ellipse, Rect, etc.
		mdl.Shape.prototype.setPath = function() {};
		// parse path into PathCommands
		mdl.Shape.prototype.updatePathCommands = function()
		{
			const actionNames = ['move','line','bezier','arc'];
			var previousPoint;
			this.pathCommands = this.path.map( function( a_path_part, a_ndx )
				{
					// a_path_part can be just vector coordinates -> { x, y, z }
					// or path instruction -> { arc: [ {x0,y0,z0}, {x1,y1,z1} ] }
					var keys = Object.keys( a_path_part );
					var method = keys[0];
					var points = a_path_part[ method ];
					// default to line if no instruction
					var isInstruction = keys.length == 1 && actionNames.indexOf( method ) != -1;
					if ( !isInstruction )
					{
						method = 'line';
						points = a_path_part;
					}
					// munge single-point methods like line & move without arrays
					var isLineOrMove = method == 'line' || method == 'move';
					var isPointsArray = Array.isArray( points );
					if ( isLineOrMove && !isPointsArray )
					{
						points = [ points ];
					}
					// first action is always move
					method = a_ndx === 0 ? 'move' : method;
					// arcs require previous last point
					var command = new mdl.PathCommand( method, points, previousPoint );
					// update previousLastPoint
					previousPoint = command.endRenderPoint;
					return command;
				}
			);
		};
		mdl.Shape.prototype.reset = function()
		{
			this.renderOrigin.set( this.origin );
			this.renderFront.set( this.front );
			// reset command render points
			this.pathCommands.forEach( function( a_cmd )
				{
					a_cmd.reset();
				}
			);
		};
		mdl.Shape.prototype.transform = function( a_tr, a_rot, a_scale )
		{
			// calculate render points backface visibility & cone/hemisphere shapes
			this.renderOrigin.transform( a_tr, a_rot, a_scale );
			this.renderFront.transform( a_tr, a_rot, a_scale );
			this.renderNormal.set( this.renderOrigin ).subtract( this.renderFront );
			// transform points
			this.pathCommands.forEach( function( a_cmd )
				{
					a_cmd.transform( a_tr, a_rot, a_scale );
				}
			);
			// transform children
			this.children.forEach( function( a_child )
				{
					a_child.transform( a_tr, a_rot, a_scale );
				}
			);
		};
		mdl.Shape.prototype.updateSortValue = function()
		{
			// sort by average z of all points
			// def not geometrically correct, but works for me
			var pointCount = this.pathCommands.length;
			var firstPoint = this.pathCommands[0].endRenderPoint;
			var lastPoint = this.pathCommands[ pointCount - 1 ].endRenderPoint;
			// ignore the final point if self closing shape
			const isSelfClosing = pointCount > 2 && firstPoint.isSame( lastPoint );
			if ( isSelfClosing )
			{
				pointCount -= 1;
			}
			var sortValueTotal = 0;
			for ( var i = 0; i < pointCount; i++ )
			{
				sortValueTotal += this.pathCommands[i].endRenderPoint.z;
			}
			this.sortValue = sortValueTotal/pointCount;
		};
		mdl.Shape.prototype.render = function( a_ctx, a_rend )
		{
			var length = this.pathCommands.length;
			if ( !this.visible || !length )
			{
				return;
			}
			// do not render if hiding backface
			this.isFacingBack = this.renderNormal.z > 0;
			if ( !this.backface && this.isFacingBack )
			{
				return;
			}
			if ( !a_rend )
			{
				throw new Error( mdl.name+': a_rend required. a_rend=' + a_rend );
			}
			// render dot or path
			var isDot = length == 1;
			if ( a_rend.isCanvas && isDot )
			{
				this.renderCanvasDot( a_ctx, a_rend );
			}
			else
			{
				this.renderPath( a_ctx, a_rend );
			}
		};
		// Safari does not render lines with no size, have to render circle instead
		mdl.Shape.prototype.renderCanvasDot = function( a_ctx )
		{
			var lineWidth = this.getLineWidth();
			if ( !lineWidth )
			{
				return;
			}
			a_ctx.fillStyle = this.getRenderColor();
			var point = this.pathCommands[0].endRenderPoint;
			a_ctx.beginPath();
			var radius = lineWidth/2;
			a_ctx.arc( point.x, point.y, radius, 0, mdl.defs.tau );
			a_ctx.fill();
		};
		mdl.Shape.prototype.getLineWidth = function()
		{
			if ( !this.stroke )
			{
				return 0;
			}
			if ( this.stroke == true )
			{
				return 1;
			}
			return this.stroke;
		};
		mdl.Shape.prototype.getRenderColor = function()
		{
			// use backface color if applicable
			var isBackfaceColor = typeof this.backface == 'string' && this.isFacingBack;
			var color = isBackfaceColor ? this.backface : this.color;
			return color;
		};
		mdl.Shape.prototype.renderPath = function( a_ctx, a_rend )
		{
			var elem = this.getRenderElement( a_ctx, a_rend );
			var isTwoPoints = this.pathCommands.length == 2 &&
			this.pathCommands[1].method == 'line';
			var isClosed = !isTwoPoints && this.closed;
			var color = this.getRenderColor();
			a_rend.renderPath( a_ctx, elem, this.pathCommands, isClosed );
			a_rend.stroke( a_ctx, elem, this.stroke, color, this.getLineWidth() );
			a_rend.fill( a_ctx, elem, this.fill, color );
			a_rend.end( a_ctx, elem );
		};
		mdl.Shape.prototype.getRenderElement = function( a_ctx, a_rend )
		{
			if ( !a_rend.isSvg )
			{
				return;
			}
			if ( !this.svgElement )
			{
				// create svgElement
				this.svgElement = document.createElementNS( mdl.defs.svgURI, 'path' );
				this.svgElement.setAttribute( 'stroke-linecap', 'round' );
				this.svgElement.setAttribute( 'stroke-linejoin', 'round' );
			}
			return this.svgElement;
		};
		/**
		 * Group
		 */
		mdl.Group=mdl.Anchor.subclass(
			{
			  updateSort: false,
			  visible: true,
			}
		);
		mdl.Group.prototype.updateSortValue = function()
		{
			var sortValueTotal = 0;
			this.flatGraph.forEach( function( a_item )
				{
					a_item.updateSortValue();
					sortValueTotal += a_item.sortValue;
				}
			);
			// average sort value of all points
			// def not geometrically correct, but works for me
			this.sortValue = sortValueTotal / this.flatGraph.length;
			if ( this.updateSort )
			{
				this.flatGraph.sort( mdl.Anchor.shapeSorter );
			}
		};
		mdl.Group.prototype.render = function( a_ctx, a_rend )
		{
			if ( !this.visible )
			{
				return;
			}
			this.flatGraph.forEach( function( a_item )
				{
					a_item.render( a_ctx, a_rend );
				}
			);
		};
		// actual group flatGraph only used inside group
		mdl.Group.prototype.updateFlatGraph = function()
		{
			// do not include self
			var flatGraph = [];
			this.flatGraph = this.addChildFlatGraph( flatGraph );
		};
		// do not include children, group handles rendering & sorting internally
		mdl.Group.prototype.getFlatGraph = function()
		{
			return [ this ];
		};
		
		/**
		 * Rect
		 */
		mdl.Rect = mdl.Shape.subclass(
			{
				width: 1,
				height: 1
			}
		);
		mdl.Rect.prototype.setPath = function()
		{
			var x = this.width / 2;
			var y = this.height / 2;
			/* eslint key-spacing: "off" */
			this.path = [
				{ x: -x, y: -y },
				{ x:  x, y: -y },
				{ x:  x, y:  y },
				{ x: -x, y:  y }
			];
		};
		
		/**
		 * RoundedRect
		 */
		mdl.RoundedRect = mdl.Shape.subclass(
			{
				width: 1,
				height: 1,
				cornerRadius: 0.25,
				closed: false,
			}
		);
		mdl.RoundedRect.prototype.setPath = function()
		{
			var xA = this.width / 2;
			var yA = this.height / 2;
			var shortSide = Math.min( xA, yA );
			var cornerRadius = Math.min( this.cornerRadius, shortSide );
			var xB = xA - cornerRadius;
			var yB = yA - cornerRadius;
			// top right corner
			var path = [
				{ x: xB, y: -yA },
				{ arc:
					[
						{ x: xA, y: -yA },
						{ x: xA, y: -yB }
					]
				},
			];
			// bottom right corner
			if ( yB )
			{
				path.push({ x: xA, y: yB });
			}
			path.push(
				{ arc:
					[
						{ x: xA, y:  yA },
						{ x: xB, y:  yA }
					]
				}
			);
		  // bottom left corner
			if ( xB )
			{
				path.push({ x: -xB, y: yA });
			}
			path.push(
				{ arc:
					[
						{ x: -xA, y:  yA },
						{ x: -xA, y:  yB }
					] 
				}
			);
			// top left corner
			if ( yB )
			{
				path.push({ x: -xA, y: -yB });
			}
			path.push(
				{ arc: 
					[
						{ x: -xA, y: -yA },
						{ x: -xB, y: -yA }
					]
				}
			);
			// back to top right corner
			if ( xB )
			{
				path.push({ x: xB, y: -yA });
			}
			this.path = path;
		};
		
		/**
		 * Ellipse
		 */
		mdl.Ellipse = mdl.Shape.subclass(
			{
				diameter: 1,
				width: undefined,
				height: undefined,
				quarters: 4,
				closed: false,
			}
		);
		mdl.Ellipse.prototype.setPath = function()
		{
			var width = this.width != undefined ? this.width : this.diameter;
			var height = this.height != undefined ? this.height : this.diameter;
			var x = width/2;
			var y = height/2;
			this.path =
			[
				{ x: 0, y: -y },
				{ arc:
					[ // top right
						{ x: x, y: -y },
						{ x: x, y: 0 },
					]
				},
			];
			// bottom right
			if ( this.quarters > 1 )
			{
				this.path.push(
					{ arc:
						[
							{ x: x, y: y },
							{ x: 0, y: y }
						]
					}
				);
			}
			// bottom left
			if ( this.quarters > 2 )
			{
				this.path.push(
					{ arc:
						[
							{ x: -x, y: y },
							{ x: -x, y: 0 }
						]
					}
				);
			}
			// top left
			if ( this.quarters > 3 )
			{
				this.path.push(
					{ arc:
						[
							{ x: -x, y: -y },
							{ x: 0, y: -y }
						]
					}
				);
			}
		};
		
		/**
		 * Polygon
		 */
		mdl.Polygon = mdl.Shape.subclass(
			{
				sides: 3,
				radius: 0.5,
			}
		);
		mdl.Polygon.prototype.setPath = function()
		{
			this.path = [];
			for ( var ndx = 0; ndx < this.sides; ndx++ )
			{
				var theta = ndx / this.sides * mdl.defs.tau - mdl.defs.tau/4;
				var x = Math.cos( theta ) * this.radius;
				var y = Math.sin( theta ) * this.radius;
				this.path.push({ x: x, y: y });
			}
		};
		
		/**
		 * Hemisphere composite shape
		 */
		mdl.Hemisphere = mdl.Ellipse.subclass(
			{
				fill: true,
			}
		);
		mdl.Hemisphere.prototype.create = function( /* options */)
		{
			// call super
			mdl.Ellipse.prototype.create.apply( this, arguments );
			// composite shape, create child shapes
			this.apex = new mdl.Anchor(
				{
					addTo: this,
					translate: { z: this.diameter / 2 },
				}
			);
			// vector used for calculation
			this.renderCentroid = new mdl.Vector();
		};
		mdl.Hemisphere.prototype.updateSortValue = function()
		{
			// centroid of hemisphere is 3/8 between origin and apex
			this.renderCentroid.set( this.renderOrigin )
			.lerp( this.apex.renderOrigin, 3/8 );
			this.sortValue = this.renderCentroid.z;
		};
		mdl.Hemisphere.prototype.render = function( a_ctx, a_rend )
		{
			this.renderDome( a_ctx, a_rend );
			// call super
			mdl.Ellipse.prototype.render.apply( this, arguments );
		};
		mdl.Hemisphere.prototype.renderDome = function( a_ctx, a_rend ) {
			if ( !this.visible )
			{
				return;
			}
			var elem = this.getDomeRenderElement( a_ctx, a_rend );
			var contourAngle = Math.atan2( this.renderNormal.y, this.renderNormal.x );
			var domeRadius = this.diameter / 2 * this.renderNormal.magnitude();
			var x = this.renderOrigin.x;
			var y = this.renderOrigin.y;
			if ( a_rend.isCanvas )
			{
				// canvas
				var startAngle = contourAngle + mdl.defs.tau/4;
				var endAngle = contourAngle - mdl.defs.tau/4;
				a_ctx.beginPath();
				a_ctx.arc( x, y, domeRadius, startAngle, endAngle );
			}
			else if ( a_rend.isSvg )
			{
				// svg
				contourAngle = ( contourAngle - mdl.defs.tau/4 ) / mdl.defs.tau * 360;
				this.domeSvgElement.setAttribute( 'd', 'M ' + -domeRadius + ',0 A ' +
					domeRadius + ',' + domeRadius + ' 0 0 1 ' + domeRadius + ',0' );
				this.domeSvgElement.setAttribute( 'transform',
					'translate(' + x + ',' + y + ' ) rotate(' + contourAngle + ')' );
			}
			a_rend.stroke( a_ctx, elem, this.stroke, this.color, this.getLineWidth() );
			a_rend.fill( a_ctx, elem, this.fill, this.color );
			a_rend.end( a_ctx, elem );
		};
		mdl.Hemisphere.prototype.getDomeRenderElement = function( a_ctx, a_rend )
		{
			if ( !a_rend.isSvg )
			{
				return;
			}
			if ( !this.domeSvgElement )
			{
				// create svgElement
				this.domeSvgElement = document.createElementNS( mdl.defs.svgURI, 'path' );
				this.domeSvgElement.setAttribute( 'stroke-linecap', 'round' );
				this.domeSvgElement.setAttribute( 'stroke-linejoin', 'round' );
			}
			return this.domeSvgElement;
		};
		
		/**
		 * Cylinder composite shape
		 */
		function noop() {}
		// ----- CylinderGroup ----- //
		mdl.CylinderGroup = mdl.Group.subclass
		(
			{
				color: '#333',
				updateSort: true,
			}
		);
		mdl.CylinderGroup.prototype.create = function()
		{
			mdl.Group.prototype.create.apply( this, arguments );
			this.pathCommands = [
				new mdl.PathCommand( 'move', [ {} ] ),
				new mdl.PathCommand( 'line', [ {} ] ),
			];
		};
		mdl.CylinderGroup.prototype.render = function( ctx, renderer )
		{
			this.renderCylinderSurface( ctx, renderer );
			mdl.Group.prototype.render.apply( this, arguments );
		};
		mdl.CylinderGroup.prototype.renderCylinderSurface = function( ctx, renderer )
		{
			if ( !this.visible )
			{
				return;
			}
			// render cylinder surface
			var elem = this.getRenderElement( ctx, renderer );
			var frontBase = this.frontBase;
			var rearBase = this.rearBase;
			var scale = frontBase.renderNormal.magnitude();
			var strokeWidth = frontBase.diameter * scale + frontBase.getLineWidth();
			// set path command render points
			this.pathCommands[0].renderPoints[0].set( frontBase.renderOrigin );
			this.pathCommands[1].renderPoints[0].set( rearBase.renderOrigin );
			if ( renderer.isCanvas ) {
			ctx.lineCap = 'butt'; // nice
			}
			renderer.renderPath( ctx, elem, this.pathCommands );
			renderer.stroke( ctx, elem, true, this.color, strokeWidth );
			renderer.end( ctx, elem );
			if ( renderer.isCanvas ) {
			ctx.lineCap = 'round'; // reset
			}
		};
		mdl.CylinderGroup.prototype.getRenderElement = function( ctx, renderer )
		{
			if ( !renderer.isSvg ) {
			return;
			}
			if ( !this.svgElement ) {
			// create svgElement
			this.svgElement = document.createElementNS( mdl.defs.svgURI, 'path' );
			}
			return this.svgElement;
		};
		// prevent double-creation in parent.copyGraph()
		// only create in Cylinder.create()
		mdl.CylinderGroup.prototype.copyGraph = noop;
		// ----- CylinderEllipse ----- //
		mdl.CylinderEllipse = mdl.Ellipse.subclass();
		mdl.CylinderEllipse.prototype.copyGraph = noop;
		// ----- Cylinder ----- //
		mdl.Cylinder = mdl.Shape.subclass({
		diameter: 1,
		length: 1,
		frontFace: undefined,
		fill: true,
		});
		mdl.Cylinder.prototype.create = function( /* options */)
		{
			// call super
			mdl.Shape.prototype.create.apply( this, arguments );
			// composite shape, create child shapes
			// CylinderGroup to render cylinder surface then bases
			this.group = new mdl.CylinderGroup(
				{
					addTo: this,
					color: this.color,
					visible: this.visible,
				}
			);
			var baseZ = this.length / 2;
			var baseColor = this.backface || true;
			// front outside base
			this.frontBase = this.group.frontBase = new mdl.Ellipse(
				{
					addTo: this.group,
					diameter: this.diameter,
					translate: { z: baseZ },
					rotate: { y: mdl.defs.tau/2 },
					color: this.color,
					stroke: this.stroke,
					fill: this.fill,
					backface: this.frontFace || baseColor,
					visible: this.visible,
				}
			);
			// back outside base
			this.rearBase = this.group.rearBase = this.frontBase.copy({
			translate: { z: -baseZ },
			rotate: { y: 0 },
			backface: baseColor,
			});
		};
		// Cylinder shape does not render anything
		mdl.Cylinder.prototype.render = function() {};
		// ----- set child properties ----- //
		var childProperties = [ 'stroke', 'fill', 'color', 'visible' ];
		for (const prop of childProperties)
		{
			// use proxy prop for custom getter & setter
			const _prop = '_' + prop;
			Object.defineProperty( mdl.Cylinder.prototype, prop,
				{
					get: function()
					{
						return this[ _prop ];
					},
					set: function( value )
					{
						this[ _prop ] = value;
						// set prop on children
						if ( this.frontBase )
						{
							this.frontBase[ prop ] = value;
							this.rearBase[ prop ] = value;
							this.group[ prop ] = value;
						}
					},
				}
			);
		}
	// TODO child property setter for backface, frontBaseColor, & rearBaseColor
		
		/**
		 * Cone composite shape
		 */
		mdl.Cone = mdl.Ellipse.subclass(
			{
				length: 1,
				fill: true,
			}
		);
		mdl.Cone.prototype.create = function( /* options */) 
		{
			// call super
			mdl.Ellipse.prototype.create.apply( this, arguments );
			// composite shape, create child shapes
			this.apex = new mdl.Anchor(
				{
				addTo: this,
				translate: { z: this.length }
				}
			);
			// vectors used for calculation
			this.renderApex = new mdl.Vector();
			this.renderCentroid = new mdl.Vector();
			this.tangentA = new mdl.Vector();
			this.tangentB = new mdl.Vector();
			this.surfacePathCommands = [
				new mdl.PathCommand( 'move', [ {} ] ), // points set in renderConeSurface
				new mdl.PathCommand( 'line', [ {} ] ),
				new mdl.PathCommand( 'line', [ {} ] ),
			];
		};
		mdl.Cone.prototype.updateSortValue = function()
		{
			// center of cone is one third of its length
			this.renderCentroid.set( this.renderOrigin )
			.lerp( this.apex.renderOrigin, 1/3 );
			this.sortValue = this.renderCentroid.z;
		};
		mdl.Cone.prototype.render = function( a_ctx, a_rend )
		{
			this.renderConeSurface( a_ctx, a_rend );
			mdl.Ellipse.prototype.render.apply( this, arguments );
		};
		
		mdl.Cone.prototype.renderConeSurface = function( a_ctx, a_rend )
		{
			if ( !this.visible )
			{
				return;
			}
			this.renderApex.set( this.apex.renderOrigin )
			.subtract( this.renderOrigin );
			var scale = this.renderNormal.magnitude();
			var apexDistance = this.renderApex.magnitude2d();
			var normalDistance = this.renderNormal.magnitude2d();
			// eccentricity
			var eccenAngle = Math.acos( normalDistance/scale );
			var eccen = Math.sin( eccenAngle );
			var radius = this.diameter / 2 * scale;
			// does apex extend beyond eclipse of face
			var isApexVisible = radius * eccen < apexDistance;
			if ( !isApexVisible )
			{
				return;
			}
			// update tangents
			var apexAngle = Math.atan2( this.renderNormal.y, this.renderNormal.x ) +
			mdl.defs.tau/2;
			var projectLength = apexDistance/eccen;
			var projectAngle = Math.acos( radius/projectLength );
			// set tangent points
			var tangentA = this.tangentA;
			var tangentB = this.tangentB;
			tangentA.x = Math.cos( projectAngle ) * radius * eccen;
			tangentA.y = Math.sin( projectAngle ) * radius;
			tangentB.set( this.tangentA );
			tangentB.y *= -1;
			tangentA.rotateZ( apexAngle );
			tangentB.rotateZ( apexAngle );
			tangentA.add( this.renderOrigin );
			tangentB.add( this.renderOrigin );
			this.setSurfaceRenderPoint( 0, tangentA );
			this.setSurfaceRenderPoint( 1, this.apex.renderOrigin );
			this.setSurfaceRenderPoint( 2, tangentB );
			// render
			var elem = this.getSurfaceRenderElement( a_ctx, a_rend );
			a_rend.renderPath( a_ctx, elem, this.surfacePathCommands );
			a_rend.stroke( a_ctx, elem, this.stroke, this.color, this.getLineWidth() );
			a_rend.fill( a_ctx, elem, this.fill, this.color );
			a_rend.end( a_ctx, elem );
		};
		mdl.Cone.prototype.getSurfaceRenderElement = function( a_ctx, a_rend ) {
			if ( !a_rend.isSvg )
			{
				return;
			}
			if ( !this.surfaceSvgElement )
			{
				// create svgElement
				this.surfaceSvgElement = document.createElementNS( mdl.defs.svgURI, 'path' );
				this.surfaceSvgElement.setAttribute( 'stroke-linecap', 'round' );
				this.surfaceSvgElement.setAttribute( 'stroke-linejoin', 'round' );
			}
			return this.surfaceSvgElement;
		};
		mdl.Cone.prototype.setSurfaceRenderPoint = function( a_ndx, a_point )
		{
			var renderPoint = this.surfacePathCommands[ a_ndx ].renderPoints[0];
			renderPoint.set( a_point );
		};
		
		/**
		 * Box composite shape
		 */
		// ----- BoxRect ----- //
		
		mdl.BoxRect = mdl.Rect.subclass();
		// prevent double-creation in parent.copyGraph()
		// only create in Box.create()
		mdl.BoxRect.prototype.copyGraph = function() {};
		
		// ----- Box ----- //
		var faceNames = [
			'frontFace',
			'rearFace',
			'leftFace',
			'rightFace',
			'topFace',
			'bottomFace',
		];
		var boxDefaults = mdl.extend( {}, mdl.Shape.defaults );
		delete boxDefaults.path;
		faceNames.forEach( function( faceName )
			{
				boxDefaults[ faceName ] = true;
			}
		);
		mdl.extend( boxDefaults,
			{
				width: 1,
				height: 1,
				depth: 1,
				fill: true,
			}
		);
		mdl.Box = mdl.Anchor.subclass( boxDefaults );
		/* eslint-disable no-self-assign */
		mdl.Box.prototype.create = function( options )
		{
			mdl.Anchor.prototype.create.call( this, options );
			this.updatePath();
			// HACK reset fill to trigger face setter
			this.fill = this.fill;
		};
		mdl.Box.prototype.updatePath = function()
		{
			// reset all faces to trigger setters
			faceNames.forEach( function( a_face_name )
				{
					this[ a_face_name ] = this[ a_face_name ];
				}, this 
			);
		};
		faceNames.forEach( function( a_face_name )
			{
				var _faceName = '_' + a_face_name;
				Object.defineProperty( mdl.Box.prototype, a_face_name,
					{
						get: function()
						{
							return this[ _faceName ];
						},
						set: function( a_val )
						{
							this[ _faceName ] = a_val;
							this.setFace( a_face_name, a_val );
						}
					}
				);
			}
		);
		mdl.Box.prototype.setFace = function( a_face_name, a_val )
		{
			var rectProperty = a_face_name + 'Rect';
			var rect = this[ rectProperty ];
			if ( !a_val )
			{
				this.removeChild( rect );
				return;
			}
			// update & add face
			var options = this.getFaceOptions( a_face_name );
			options.color = typeof a_val == 'string' ? a_val : this.color;
			if ( rect )
			{
				// update previous
				rect.setOptions( options );
			}
			else
			{
				// create new
				rect = this[ rectProperty ] = new mdl.BoxRect( options );
			}
			rect.updatePath();
			this.addChild( rect );
		};
		mdl.Box.prototype.getFaceOptions = function( a_face_name )
		{
			return{
				frontFace:
				{
					width: this.width,
					height: this.height,
					translate: { z: this.depth / 2 },
				},
				rearFace:
				{
					width: this.width,
					height: this.height,
					translate: { z: -this.depth / 2 },
					rotate: { y: mdl.defs.tau/2 },
				},
				leftFace:
				{
					width: this.depth,
					height: this.height,
					translate: { x: -this.width / 2 },
					rotate: { y: -mdl.defs.tau/4 },
				},
				rightFace:
				{
					width: this.depth,
					height: this.height,
					translate: { x: this.width / 2 },
					rotate: { y: mdl.defs.tau/4 },
				},
				topFace:
				{
					width: this.width,
					height: this.depth,
					translate: { y: -this.height / 2 },
					rotate: { x: -mdl.defs.tau/4 },
				},
				bottomFace:
				{
					width: this.width,
					height: this.depth,
					translate: { y: this.height / 2 },
					rotate: { x: mdl.defs.tau/4 },
				},
			}[ a_face_name ];
		};
		var childProperties = [ 'color', 'stroke', 'fill', 'backface', 'front',
			'visible' ];
		childProperties.forEach( function( a_prop )
			{
				// use proxy a_prop for custom getter & setter
				var _prop = '_' + a_prop;
				Object.defineProperty( mdl.Box.prototype, a_prop,
					{
						get: function()
						{
							return this[ _prop ];
						},
						set: function( value )
						{
							this[ _prop ] = value;
							faceNames.forEach( function( a_face_name )
								{
									var rect = this[ a_face_name + 'Rect' ];
									var isFaceColor = typeof this[ a_face_name ] == 'string';
									var isColorUnderwrite = a_prop == 'color' && isFaceColor;
									if ( rect && !isColorUnderwrite )
									{
										rect[ a_prop ] = value;
									}
								},
								this
							);
						}
					}
				);
			}
		);
		/*Zdog compatability*/
		window.Zdog=mdl;
		Zdog.TAU=mdl.defs.tau;
		/*end Zdog compatability*/
		/**/
		const me = mdl.name;
		spaho.mdls[mdl.key] = mdl;
		//console.log(me,'added self to spaho.mdls.'+mdl.key,spaho.mdls[mdl.key]);
		document.dispatchEvent(spaho.events.loaded);
		/**/
	}.bind({bind_curr_script:window.document.currentScript.src})
);
