/*!
 * Spaho
 * Copyright 2023 Alexander Gribkov
 * https://codeberg.org/sallecta/spaho
 * Licensed MIT
 * 
 * based on
 * Zdog v1.1.3
 * Round, flat, designer-friendly pseudo-3D engine
 * Licensed MIT
 * https://zzz.dog
 * Copyright 2020 Metafizzy
 * 
 * AnchorJS
 * MIT License
 * Copyright (c) 2021 Bryan Braun
 * https://github.com/bryanbraun/anchorjs

 */
"use strict";
window.spaho = {};
spaho.defs={};
spaho.defs.version="0.1.2";
Object.freeze(spaho.defs);
//
spaho.cfg={};
spaho.cfg.factoryfont="SourceSans3_Regular_ttf";
spaho.cfg.perspective=false;
//
spaho.version = '0.0.2';//https://semver.org/
spaho.name = 'spaho';
spaho.resources = [];
spaho.resources.push({type:'css',id:'spaho',});
spaho.resources.push({type:'module',id:'core',});
spaho.resources.push({type:'library',id:'font',});
spaho.resources.push({type:'plugin',id:'text',});
spaho.resmdls_total=0;
spaho.reslibs_total=0;
spaho.resplgs_total=0;

spaho.mdls={}; // mules container
spaho.plgs={}; // plugins container
spaho.libs={}; // libraries container
spaho.load_err = false;
spaho.events = {};
spaho.dirname = function( a_path,a_levels=1 )
{
	const me=spaho.name+'.dirname';
	let path_len = a_path.length;
	let out;
	let level=1;
	for (let ndx = path_len-1; ndx > -1; ndx--)
	{
		if ( a_path.charAt(ndx-1) === '/')
		{
			if ( level === a_levels )
			{
				////c0onsole.log('level === a_levels',level,a_levels);
				out = a_path.slice(0,ndx-1);
				break;
			}
			////c0onsole.log('level != a_levels',level,a_levels);
			level = level + 1;
		}
	}
	return out;
} // spaho.dirname

spaho.path = spaho.dirname(window.document.currentScript.src);

// Create the events.
spaho.events.included = document.createEvent("Event");
spaho.events.included.name = 'spaho.included';
spaho.events.included.initEvent(spaho.events.included.name, true, true);
//
spaho.events.loaded = document.createEvent("Event");
spaho.events.loaded.name = 'spaho.checkloaded';
spaho.events.loaded.initEvent(spaho.events.loaded.name, true, true);
//
spaho.events.ready = document.createEvent("Event");
spaho.events.ready.name = 'spaho.ready';
spaho.events.ready.initEvent(spaho.events.ready.name, true, true);
//
spaho.include = function()
{
	const me = 'spaho.include';
	
	function add_resource(a_res, a_fn)
	{
		const me = 'spaho.include.add_resource';
		let el;
		const id = a_res.id;
		if ( a_res.type === 'module' )
		{
			el = document.createElement('script');
			el.src = spaho.path+'/modules/'+id+'/spaho.mdl.'+id+'.js'; 
			el.type = 'text/javascript';
			el.defs={tp:a_res.type};
		}
		else if ( a_res.type === 'library' )
		{
			el = document.createElement('script');
			el.src = spaho.path+'/libs/'+id+'/spaho.lib.'+id+'.js'; 
			el.type = 'text/javascript';
			el.defs={tp:a_res.type};
		}
		else if ( a_res.type === 'plugin' )
		{
			el = document.createElement('script');
			el.src = spaho.path+'/plugins/'+id+'/spaho.plg.'+id+'.js'; 
			el.type = 'text/javascript';
			el.defs={tp:a_res.type};
		}
		else if ( a_res.type === 'css' )
		{
			el = document.createElement('link');
			el.rel = 'stylesheet'; 
			el.href = spaho.path+'/css/'+id+'.css'; 
			el.type = 'text/css';
			el.defs={tp:a_res.type};
		}
		else
		{
			console.warn(me+': unknow res: '+a_res.type);
			return 1;
		}
		el.id = id;
		el.addEventListener('error',a_fn);
		el.addEventListener('load',a_fn);
		document.head.appendChild(el);
	}
	
	function next(a_ev)
	{
		spaho.cnt = spaho.cnt + 1;
		if ( a_ev.type === 'load')
		{
			//c0onsole.log(me,spaho.cnt,' resource',a_ev.target.id,'included',JSON.stringify(spaho.mdls));
			if ( spaho.resources[spaho.cnt] )
			{
				////c0onsole.log( me,spaho.cnt,' Including resource',spaho.resources[spaho.cnt] );
				add_resource(spaho.resources[spaho.cnt],next);
			}
			else
			{
				////c0onsole.log(me,spaho.cnt,'All spaho resources included.');
				document.dispatchEvent(spaho.events.included);
			}
		}
		else if ( a_ev.type === 'error' )
		{
			spaho.load_err = true;
			console.error(me,spaho.cnt,' resource',a_ev.target.id,'of type',a_ev.target.defs.tp,'not loaded');
		}
		else
		{
			spaho.load_err = true;
			console.error(me,spaho.cnt,'unknown result on resource',a_ev.id,a_ev.type);
		}
	};
	
	spaho.cnt = 0;
	add_resource(spaho.resources[spaho.cnt],next);
}
spaho.include();

spaho.included = function(a_ev)
{
	const me = 'spaho.included';
	
	for(let ndx=0; ndx < spaho.resources.length; ndx++)
	{
		const item = spaho.resources[ndx];
		if ( item.type == 'module')
		{
			spaho.resmdls_total = spaho.resmdls_total + 1;
			//c0onsole.log(me, '-- counted as module',item);
		}
		else if ( item.type == 'library')
		{
			spaho.reslibs_total = spaho.reslibs_total + 1;
			//c0onsole.log(me, '-- counted as library',item);
		}
		else if ( item.type == 'plugin')
		{
			spaho.resplgs_total = spaho.resplgs_total + 1;
			//c0onsole.log(me, '-- counted as plugin',item);
		}
		//else
		//{
			//console.warn(me, '-- not counted unknown item',item);
		//}
	}
}

spaho.checkloaded = function(a_ev)
{
	const me = 'spaho.checkloaded';
	const mdls_total = Object.keys(spaho.mdls).length;
	const libs_total = Object.keys(spaho.libs).length;
	const plgs_total = Object.keys(spaho.plgs).length;
	if ( mdls_total !== spaho.resmdls_total || libs_total !== spaho.reslibs_total || plgs_total !== spaho.resplgs_total )
	{
		//console.warn(me+': app is not ready:');
		//console.warn(' -- '+me+': mdls_total='+mdls_total+', spaho.resmdls_total='+spaho.resmdls_total);
		//console.warn(' -- '+me+': libs_total='+libs_total+', spaho.reslibs_total='+spaho.reslibs_total);
		//console.warn(' -- '+me+': plgs_total='+plgs_total+', spaho.resplgs_total='+spaho.resplgs_total);
	}
	else
	{
		//c0onsole.log(me+': app is ready:');
		//c0onsole.log(' -- '+me+': mdls_total='+mdls_total+', spaho.resmdls_total='+spaho.resmdls_total);
		//c0onsole.log(' -- '+me+': libs_total='+libs_total+', spaho.reslibs_total='+spaho.reslibs_total);
		//c0onsole.log(' -- '+me+': plgs_total='+plgs_total+', spaho.resplgs_total='+spaho.resplgs_total);
		document.dispatchEvent(spaho.events.ready);
	}
}

spaho.ready = function(a_ev)
{
	const me = 'spaho.ready';
}
document.addEventListener( spaho.events.included.name, spaho.included,false );
document.addEventListener( spaho.events.loaded.name, spaho.checkloaded,false );
document.addEventListener( spaho.events.ready.name, spaho.ready,false );

